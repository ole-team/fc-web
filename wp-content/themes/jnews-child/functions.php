<?php

/**
 * Load parent theme style
 */
add_action( 'wp_enqueue_scripts', 'jnews_child_enqueue_parent_style' );

function jnews_child_enqueue_parent_style()
{
    wp_enqueue_style( 'jnews-parent-style', get_parent_theme_file_uri('/style.css'));
}
function get_post_primary_category( $post = 0, $taxonomy = 'category' ){
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}
function appAD_fetch_embed( $cached_html, $url, $attr, $post_id ) {
    if ( false !== strpos( $url, "://twitter.com")){
        $pattern  = '#https?://twitter\.com/(?:\#!/)?(\w+)/status(es)?/(\d+)#is';
        preg_match( $pattern, $url, $matches );
        $urlt = $matches[0];
        $urltw = 'https://twitframe.com/show?url='.$urlt;
        $cached_html = '<div class="embed" style="max-width:500px;"><iframe border="0" frameborder="0" height="410" width="100%" src="'.$urltw.'"></iframe></div>';
    }
    return $cached_html;
}
add_filter('embed_oembed_html','appAD_fetch_embed', 10, 4 );

//Allow disable Facebook Instant Article
add_filter("instant_articles_should_submit_post", "ad_validation_FBIA",10, 2);
function ad_validation_FBIA($should_show, $IA_object){
    $validateIA = get_post_meta($IA_object->get_the_id(), "disable_ia");
    if($validateIA){
        return false;
    }else{
        return true;
    }   
}
add_action('post_submitbox_misc_actions', 'create_disable_ia');
add_action('save_post', 'save_disable_ia');
add_action('post_submitbox_misc_actions', 'create_liveblog');
add_action('save_post', 'save_liveblog');
function create_disable_ia(){
    $post_id = get_the_ID();
  
    if (get_post_type($post_id) != 'post') {
        return;
    }
  
    $value = get_post_meta($post_id, 'disable_ia', true);
    wp_nonce_field('ad_disable_ia_nonce_'.$post_id, 'ad_disable_ia_nonce');
    ?>
    <div class="misc-pub-section misc-pub-section-last">
        <label><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="disable_ia" /><?php _e('Disable this post in FB-IA', 'pmg'); ?></label>
    </div>
    <?php
}
function save_disable_ia($post_id){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    
    if (
        !isset($_POST['ad_disable_ia_nonce']) ||
        !wp_verify_nonce($_POST['ad_disable_ia_nonce'], 'ad_disable_ia_nonce_'.$post_id)
    ) {
        return;
    }
    
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    if (isset($_POST['disable_ia'])) {
        update_post_meta($post_id, 'disable_ia', $_POST['disable_ia']);
    } else {
        delete_post_meta($post_id, 'disable_ia');
    }
}

function user_can_richedit_custom() {
    global $wp_rich_edit;
    if (get_user_option('rich_editing') == 'true' || !is_user_logged_in()) {
        $wp_rich_edit = true;
        return true;
    }
    $wp_rich_edit = false;
    return false;
}

add_filter('user_can_richedit', 'user_can_richedit_custom');

add_shortcode('videoDLM', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    $typeDLM = $atts['type'];
    if(!empty($idDLM)){
        $html .= '<div class="video_wrapper_responsive vd-'.$typeDLM.'"><div id="'.$typeDLM.'-DLM" data-id="'.$idDLM.'"></div></div>';
    wp_enqueue_script('DMapi', 'https://api.dmcdn.net/all.js');
    }  
    return $html;
});

function videos_content( $content ) {
    if ( is_single() && 'post' == get_post_type() ) {

        $custom_content = "";

        $custom_content .= $content;
        $live = false;
        $v_DLM = get_post_meta( get_the_ID(), 'video_destacado', true );
        $v_DLP = get_post_meta( get_the_ID(), 'video_recomendado', true );
        
        
        $post_tags = get_the_tags( get_the_ID());
        if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {
            if(!empty($v_DLM)){
                $precontent = '<amp-dailymotion data-videoid="'.$v_DLM.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion></br>';
                $custom_content = $precontent.$content;
            }
            if(!empty($v_DLP)){
                $custom_content .= '<strong>Te puede interesar: </strong><br />';
                $custom_content .= '<amp-dailymotion data-videoid="'.$v_DLP.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion>';
                
            }
        }else{
            if( has_tag( 'branded' ) ) {
            }else{
            if(!empty($v_DLP)){
                $custom_content .= '<strong>Te puede interesar: </strong><br />';
                $custom_content .= do_shortcode('[videoDLM id="'.$v_DLP.'" type="recomendado" title="'.$tt_recomendado.'"]');
                }
            }
        }
        return $custom_content;
    } else {
        return $content;
    }
}
add_filter( 'the_content', 'videos_content' );

add_action('wp_head', 'customSetTargetting',1);

function customSetTargetting(){
    $tags = '<script>';
    if(is_single()) {
        $type_post = 'news';
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category'); 
        $dmVideo = get_post_meta($tempID, 'video_destacado', true );
        $post_author_id = get_post_field( 'post_author', $tempID );
        $autor_email = get_the_author_meta('nicename',$post_author_id);
        if(!empty($autor_email)){
            $tags .= 'var autorET = "'.$autor_email.'";';
        }
        if(!empty($prm_ct)){
            $tags .= 'var primaryCat = "'.$prm_ct['slug'].'";';
        }
        $categoriesp = get_the_category();
        if(!empty($categoriesp)){
            $varcats = '';
            foreach( $categoriesp as $ctp ) {
                $varcats .=  "'".$ctp->name."',";
            }
            $tags .= 'var post_categories = ['.$varcats.'];';
        }
        if(!empty($dmVideo) or has_tag('video')){
            $type_post = "video";   
        }elseif(has_tag('radio')){
            $type_post = "radio";   
        }elseif(has_tag('podcast')){
            $type_post = "podcast";   
        }
        $tags .= 'var tt_content = "'.$type_post.'";'; 
        $vertical = 'entretenimiento';
        $tags .= 'var tt_tag = "'.$vertical.'";';
    }
    if(is_category()){
        $pc = 'var post_categories = ["categoria"];';
        $cate = get_the_category();
        $cat_name = $cate[0]->slug;
        if(!empty($cat_name)){
            $pc = 'var post_categories = ["'.$cat_name.'"];';
        }
        $tags .= 'var tt_content = "page";'.$pc.' var tt_tag = "home";'; 
    }
    if (is_page() && !is_front_page()){
        $slug = 'page';
        $slug = rtrim(get_permalink(), '/');
        $slug = basename($slug);
        $tags .= 'var tt_content = "page"; var primaryCat = "page"; var tt_tag ="'.$slug.'";'; 
    } 
    if (is_front_page()){
        $tags .= 'var tt_content = "home"; var post_categories = ["home"]; var tt_tag = "home";'; 
    } 
    $tags .= '</script>';
    echo $tags;
}
function remove_unused_lib() {
    if (!is_admin()) {
        wp_dequeue_style('js_composer_front');
        wp_dequeue_script('comment-reply');
        wp_deregister_style('js_composer_front');
     }
    if(!is_page('contacto')){
        wp_dequeue_style('contact-form-7');
        wp_dequeue_script('contact-form-7');
    }
    if(!is_single()){
        wp_dequeue_style('jnews-previewslider');
        wp_dequeue_style('jnews-previewslider-responsive');
        wp_dequeue_script('jnews-autoload');
        wp_deregister_style('jnews-previewslider');
        wp_deregister_style('jnews-previewslider-responsive');
        wp_dequeue_style('jnews-food-recipe-css');
    }
    wp_dequeue_style('wp-mediaelement');
    wp_dequeue_script('wp-mediaelement');
    wp_deregister_script('wp-mediaelement');
    wp_deregister_style('wp-mediaelement');
}

add_action( 'wp_enqueue_scripts', 'remove_unused_lib' );

function custom_embed_settings($code){
    if(strpos($code, 'dai.ly') !== false || strpos($code, 'dailymotion.com') !== false){
        $return = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2?queue-autoplay-next=false&queue-enable=false&sharing-enable=false", $code);
        return '<div class="video_wrapper_responsive embed">'.$return.'</div>';
    }
    if(strpos($code, 'tiktok.com') !== false){
        return '<div class="embed">'.$code.'</div>';
    }
    return $code;

}

add_filter('embed_handler_html', 'custom_embed_settings');
add_filter('embed_oembed_html', 'custom_embed_settings');

add_action('wp_head', 'adOPs_calls',10);
function adOPs_calls(){
    $adTagCall = '';
    $typep = '';
    $varcontrol = 1;
    if(is_single()){
        if(has_tag('noads') or has_tag('branded')){
            $varcontrol  = 0;
        }
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category');
        if(!empty($prm_ct)){
            $temp_section = $prm_ct['slug'];
            $section = $temp_section;

        }
        $typep = 'nodes';
    }
    if(is_page()){
        if(is_page( array( 5980,15277,6242))){
            $varcontrol = 0;
        }else{
            $varcontrol = 1;
            $typep = 'page';
        }
        
    }
    if(is_404()){
        $varcontrol  = 0;
    }
    if($varcontrol == 1){
        $tags = '<!--'.$varcontrol.'--><script>';
        $tags .= '(function($) {
        var sizesScroll = [[728, 90],[970, 90]];
        var sizesTop = [[728, 90],[970, 90]];
        var sizesMid = [[728, 90],[728,250]];
        var sizesSqr1 = [300,250];
        var sizesSqr6 = [[728, 90],[970, 90]];
        var sizesFoot = [[728, 90],[970, 90],[1,1]];
        if ($(window).width() < 720) {
            sizesScroll = [320,50];
            sizesTop = [[320,100],[320,50]];
            sizesMid = [[320,50],[300,250],[300,600]];
            sizesSqr1 = [[320, 100],[320, 50],[300, 250],[300,600]];
            sizesSqr6 = [[320, 100],[320, 50],[300,600]];
            sizesFoot = [[320, 50],[1,1]];
        }
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() { '; 
        $tags .= 'googletag.defineSlot("/4923229/scrollbanner_mid_728x90_970x90_300x250_320x50", sizesScroll, "scrollbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/top_banner_atf_728x90_970x90_320x50_320x100", sizesTop, "topbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/midbanner_mid_320x50_300x250_336x280_728x90_728x250", sizesMid, "midbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/sqrbanner1_atf_300x250_336x280_300x600", sizesSqr1, "sqrbanner1").addService(googletag.pubads());
        googletag.defineSlot("/4923229/sqrbanner6_mid_320x50_300x250_336x280_728x90_728x250", sizesSqr6, "sqrbanner6").addService(googletag.pubads());
        googletag.defineSlot("/4923229/footbanner_btf_728x250_728x90_970x90_300x250", sizesFoot, "footbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/nativeintext_1x1",[1,1], "overlay_1x1").addService(googletag.pubads());';
        if(is_single()){
            $tags .= 'googletag.defineSlot("/4923229/native_intext_1x1",[1,1], "intext_1x1").addService(googletag.pubads());';
        }
        if(is_front_page()){
            $typep = 'home';    
        }
        if(is_category()){
            $catSite = get_the_category();
            if ( ! empty( $catSite ) ) {
                $temp_section  = $catSite[0]->slug; 
                $section = $temp_section;
            }
            $typep = 'category';    
        }
        $tags .= 'googletag.pubads().setTargeting("site", "futbolclub");';
        if(is_front_page()){
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "home");';
        }else{
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "'.$typep.'");';
            $tags .= 'googletag.pubads().setTargeting("category", "'.$section.'");';
        }
        $tags .= 'googletag.pubads().collapseEmptyDivs();';
        $tags .= 'var SECONDS_TO_WAIT_AFTER_VIEWABILITY = 30;
        googletag.pubads().addEventListener("impressionViewable", function(event) {
            var slot = event.slot;
            if (slot.getTargeting("refresh").indexOf("true") > -1) {
            setTimeout(function() {
                googletag.pubads().refresh([slot]);
            }, SECONDS_TO_WAIT_AFTER_VIEWABILITY * 1000);
            }
        });
        googletag.pubads().addEventListener("slotRenderEnded", function(event) {
            if (event.slot.getSlotElementId() == "footbanner") {
                $("#wrap_footer_banner").fadeIn("fast");  
            }
        });';
        $tags .= 'googletag.enableServices(); }); })(jQuery);';
        $tags .= '</script>';
        echo $tags;
    }
}
function add_meta_robots_tags($robots) {
    if(is_single()){
        if(has_tag('exclude')){
            return 'noindex, nofollow';
        }
    }
    return $robots;
}
add_filter("wpseo_robots", 'add_meta_robots_tags');